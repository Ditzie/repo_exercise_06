﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
        public class NumberChecker : MonoBehaviour
    {

        public int[] numbers;
        public int numberToSkip;
        public int numberToBreakOut;

        private void LoopArray()
        {
            //numbers.lenght= size of array
            for (int i = 0; i < numbers.Length; i++)
            {
                if(numberToBreakOut==numbers[i])
                {//just to show that something is happening
                    Debug.Log("Return");
                    break;
                }
                if(numberToSkip==numbers[i])
                { 
                    Debug.Log("I dont like this one");
                }
                else {
                    Debug.Log(numbers[i]);
                }
            }

            Debug.Log("Job done.");
        }

        private void DoSomething(int thisIsAnArgument)
        { //runs through array
            for (int i = 0; i < numbers.Length; i++)
            {
                if(numbers[i]==thisIsAnArgument)
                {
                    Debug.Log("Skip this one");
                
                }

                else
                {
                    Debug.Log(numbers[i]);
                }
            }

            Debug.Log("Im DONE");
        }

        private void AnotherMethod(int parameter, int factor)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i]==parameter)
                {
                    Debug.Log("Skip it");
                }

                else
                {
                    Debug.Log(numbers[i]);
                }
                
                if (numbers[i]== factor)
                {
                    Debug.Log("Aight,Imma head out..");

                    break;
                }
                Debug.Log("Enough Loops for today");
            }
        }
        void Start()
        {
            LoopArray();
            DoSomething(2);
            AnotherMethod(7,8);
        }

    
    }
}

